

// function showIntensityAlert(windSpeed){
// 	try {
// 		alert(determineTyphoonIntensity(windSpeed));
// 	} catch (error) {

// 		console.log(typeof error);
// 		console.log(error.message);
// 	} finally {
// 		alert('Intensify updates will show new alert.');
// 	}
	
// 	} 

// 	showIntensityAlert(56);

// const num = 100, x = 'a';

// try {
// 	console.log(num/x);
// 	console.log(a);
// } catch (error) {
// 	console.log('An error caught.');
// 	console.log('Error message: ' + error);
// } finally {
// 	alert('Finally will execute.')
// }

// Mini Activity

// function gradeEvaluator(grade){
// 	try {
// 		if (grade >= 90) {
// 			grade = "A";
// 		} else if (grade >= 80) {
// 			grade = "B";
// 		} else if (grade >= 71) {
// 			grade = "C";
// 		} else if (grade <= 70)	{
// 			grade ="F";
// 		} else if (typeof grade !== "number"){
// 			throw Error("Invalid input")
// 		}
// 		console.log(grade)
// 	} catch (error) {
// 		console.log(error)
		
// 	} finally {
// 		alert("Done initializing gradeEvaluator.");
// 	}
// }
// gradeEvaluator("a");
// gradeEvaluator(8);

function displayMsgToSelf(){
	console.log("Hello! I am from the future.")
}

let count = 10;

while(count !== 0) {
	displayMsgToSelf();
	count--;
}
// While loop - allow us to repeat an action or an instruction
// as long as the condition is true

// 1st loop - count 10
// 2nd loop - count 9
// 3rd loop - count 8
// 4th loop - count 7
// 5th loop - count 6
// 6th loop - count 5
// 7th loop - count 4
// 8th loop - count 3
// 9th loop - count 2
// 10th loop - count 1

// While loop checked if count is still NOT equal to 0:
// at this point, before a possible 11th loop, count is decremented is 0

// if there is no decrementration, the condition is always true,
// thus, an infinite loop

// Infinite loops will run your code block forever until you stop it.
// infinite loop is a piece of code that keeps running because a terminating condition is never reached.
// Samples:
// 	while(true) {
// 	console.log('This will result as infinite loop.')
// }

// Always make sure that at the very least, your loop condition 
// will be terminated or will be false at one point


// Mini Activity
function displayCount (){
	console.log(number)
}
 let number = 1;
 while(number !== 16) {
 	displayCount();
 	number++;
 }


// Do While Loop
	// Do While loop is similar to While loop.
	// However, Do While loop will run the code block at least once
	// before it checks the condition

	// With While loop, we first check the condition and
		// then run the code block/statements
// let doWhileCoutner = 20

// do {
// 	console.log(doWhileCoutner)
// 	--doWhileCoutner
// } while (doWhileCoutner < 0);


// For loop - more flexible than while
// and do-while loops.

// Consists of 3 parts:
// 1. The "initialization" value that will
	// track the progression of the loop
// 2. The "expression/condition" that will be
	// evalluated which will determine whether 
	// the loop will run one more time.
// The "finexExpression" indicated how to advance the loop

// Syntax:

	// for(initialization, condition, finalExpression){
	 	//codeblock
	// }

// Create a loop that will start from 1 and end at 20

for(let x = 1; x <= 20; x++){
	console.log(x)
}

// Can we use for loop in a string?
let name = "Nikko Hey";

//Accessing elements of a string
	// using index
	// index starts at 0

// Use for loop to display each character of the string
for(let index = 0; index < name.length; index++){
	console.log(name[index])
}

// Using for loop in an array
let fruits = ["mango", "apple", "orange", "banana", "strawberry", "kiwi"]
// elements - each value inside the array
console.log(fruits)

// Total count of elements in an array
console.log(fruits.length)

// Access each element in an array
// we use index[] 
console.log(fruits[0])
console.log(fruits[4]) 
console.log(fruits[fruits.length - 1])
// console.log(fruits[6])

// if we want to assign new value to an element
// assignment operator
fruits[6] = "grapes"
console.log(fruits)

// Using for loop in an array
for (let i = 0; i < fruits.length; i++){
	console.log(fruits[i])
}

// Example of Array of objects
let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}	
];

console.log(cars.length);
console.log(cars[1]);

// Use for loop to display each element in an array

for (let y = 0; y < cars.length; y++){
	console.log(cars[y])
}

function displayName() {
	let myName = "Bianca"
	for (i = 0; i < myName.length; i++){
		if (myName[i] === "a") {
			console.log(3)
		} else if (myName[i]==="i") {
			console.log(3)
		} else if (myName[i]==="e") {
			console.log(3)
		} else if (myName[i]==="o") {
			console.log(3)
		} else if (myName[i]==="u") {
			console.log(3)
		} else {
			console.log(myName[i])
		}
	}
}
displayName();


// Continue and Break
	// The "Continue" statement
	// console.log(100/3)
	// 33.33
	// console.log(100%3) //1
for (let a = 20; a > 0; a--){
	if (a%2 === 0){
		continue
	}

	if(a < 10){
		break
	}
	console.log(a)
}

// Mini Activity


// function displayLoop(){
// 	let myName = "Bianca"
// 	for (let i = 0; i < myName.length; i++){
// 		if (myName[i].toLowerCase() === "a"){
// 			+ 1 
// 			continue
// 		}


// 		if (myName[i].toLowerCase() === "d"){
// 			break
// 		}
// 		console.log(myName[i])
// 	} 
// }

let string = "alexandra"
for(let i = 0; i < string.length; i++){
	if (string[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration")
			continue; 
	}

	if(string[i].toLowerCase() === "d") {
	break;
	}
}

// Is it possible to have another loop inside a loop?
// Nested Loops

for (let x = 0; x <= 10; x++){
	console.log(x)

	for(let y = 0; y <= 10; y++){
		console.log(`${y} = ${x+y}`)
	}
}







